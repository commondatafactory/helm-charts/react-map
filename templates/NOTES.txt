===
{{ if .Values.dego.enabled }}
DEGO can be accessed via port 8080 on the following DNS name from within your cluster:
{{ template "react-map.dego.fullname" . }}.{{ .Release.Namespace }}.svc.cluster.local
--
{{ if .Values.dego.ingress.enabled -}}
From outside the cluster, the server URL(s) are:
{{ range .Values.dego.ingress.hosts }}
https://{{ . }}
{{- end }}
{{- end }}
{{- end }}
===
{{ if .Values.dook.enabled }}
DOOK can be accessed via port 8080 on the following DNS name from within your cluster:
{{ template "react-map.dook.fullname" . }}.{{ .Release.Namespace }}.svc.cluster.local
--
{{ if .Values.dook.ingress.enabled -}}
From outside the cluster, the server URL(s) are:
{{ range .Values.dook.ingress.hosts }}
https://{{ . }}
{{- end }}
{{- end }}
{{- end }}
===
