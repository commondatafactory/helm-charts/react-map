{{/*
Expand the name of the chart.
*/}}
{{- define "react-map.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "react-map.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create unified labels for react-map components
*/}}
{{- define "react-map.common.matchLabels" -}}
app: {{ template "react-map.name" . }}
release: {{ .Release.Name }}
{{- end -}}

{{- define "react-map.common.metaLabels" -}}
chart: {{ template "react-map.chart" . }}
heritage: {{ .Release.Service }}
{{- end -}}

{{- define "react-map.dego.labels" -}}
{{ include "react-map.dego.matchLabels" . }}
{{ include "react-map.common.metaLabels" . }}
{{- end -}}

{{- define "react-map.dego.matchLabels" -}}
component: {{ .Values.dego.name | quote }}
{{ include "react-map.common.matchLabels" . }}
{{- end -}}

{{- define "react-map.dook.labels" -}}
{{ include "react-map.dook.matchLabels" . }}
{{ include "react-map.common.metaLabels" . }}
{{- end -}}

{{- define "react-map.dook.matchLabels" -}}
component: {{ .Values.dook.name | quote }}
{{ include "react-map.common.matchLabels" . }}
{{- end -}}

{{- define "react-map.demo.labels" -}}
{{ include "react-map.demo.matchLabels" . }}
{{ include "react-map.common.metaLabels" . }}
{{- end -}}

{{- define "react-map.demo.matchLabels" -}}
component: {{ .Values.demo.name | quote }}
{{ include "react-map.common.matchLabels" . }}
{{- end -}}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "react-map.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a fully qualified dego name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}

{{- define "react-map.dego.fullname" -}}
{{- if .Values.dego.fullnameOverride -}}
{{- .Values.dego.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.dego.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.dego.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a fully qualified dook name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "react-map.dook.fullname" -}}
{{- if .Values.dook.fullnameOverride -}}
{{- .Values.dook.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.dook.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.dook.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{/*
Create a fully qualified demo name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "react-map.demo.fullname" -}}
{{- if .Values.demo.fullnameOverride -}}
{{- .Values.demo.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.demo.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.demo.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Return the appropriate apiVersion for deployment.
*/}}
{{- define "deployment.apiVersion" -}}
{{- print "apps/v1" -}}
{{- end -}}
{{/*

{{/*
Return the appropriate apiVersion for ingress.
*/}}
{{- define "ingress.apiVersion" -}}
{{- print "networking.k8s.io/v1" -}}
{{- end -}}

{{/*
Define the react-map.namespace template if set with forceNamespace or .Release.Namespace is set
*/}}
{{- define "react-map.namespace" -}}
{{- if .Values.forceNamespace -}}
{{ printf "namespace: %s" .Values.forceNamespace }}
{{- else -}}
{{ printf "namespace: %s" .Release.Namespace }}
{{- end -}}
{{- end -}}
